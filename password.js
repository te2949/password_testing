const checklength = function (password) {
  return password.length >= 8 && password.length <= 25
}

const checkAlphabet = function (password) {
  const alphabets = 'abcdefghijklmnopqrstuvwxyz'
  for (const ch of password) {
    if (alphabets.includes(ch.toLowerCase())) return true
  }
  return false
}
const checkDigit = function (password) {
  const digit = '0123456789'
  for (const ch of password) {
    if (digit.includes(ch.toLowerCase())) return true
  }
  return false
}
const checkSymbol = function (password) {
  const symbol = '!"#$%&()*+,-./:;<=>?@[]^_`{|}~'
  for (const ch of password) {
    if (symbol.includes(ch.toLowerCase())) return true
  }
  return false
}

const checkPassword = function (password) {
  return checklength(password) &&
  checkAlphabet(password) &&
  checkDigit(password) &&
  checkSymbol(password)
}
module.exports = {
  checklength,
  checkAlphabet,
  checkDigit,
  checkSymbol,
  checkPassword
}
