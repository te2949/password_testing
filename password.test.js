const { checklength, checkAlphabet, checkSymbol, checkPassword, checkDigit } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checklength('12345678')).toBe(true)
  })
  test('should 7 characters to be false', () => {
    expect(checklength('1234567')).toBe(false)
  })
  test('should 25 characters to be true', () => {
    expect(checklength('1111111111111111111111111')).toBe(true)
  })
  test('should 26 characters to be false', () => {
    expect(checklength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password ', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A in password ', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has not alphabet in password ', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has Digit 3 in password to be true', () => {
    expect(checkDigit('3')).toBe(true)
  })
  test('should has not Digit  in password to be false', () => {
    expect(checkDigit('xxxx')).toBe(false)
  })
})
describe('Test Symbol', () => {
  test('should has symbol / in password to be true', () => {
    expect(checkSymbol('11/11')).toBe(true)
  })
  test('should has not symbol in password to be false', () => {
    expect(checkSymbol('1111')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password Va.789456 to be true', () => {
    expect(checkPassword('Va.789456')).toBe(true)
  })
  test('should password Va.313 to be false', () => {
    expect(checkPassword('Va.313')).toBe(false)
  })
  test('should password Va12345678 to be false', () => {
    expect(checkPassword('Va12345678')).toBe(false)
  })
  test('should password Va.abced to be false', () => {
    expect(checkPassword('Va.abcde')).toBe(false)
  })
})
